# STARTX infrastructure Ansible collection

This is the main documentation for the `startxfr.infra` ansible collection.

## Roles

| version                                        | Description                                  |
| ---------------------------------------------- | -------------------------------------------- |
| [awx_codecommit role](roles/awx_codecommit/)   | Install or uninstall a codecommit repository |
| [network_openvpn role](roles/network_openvpn/) | Install or uninstall a openvpn service       |
| [network_squid role](roles/network_squid/)     | Install or uninstall a squid service         |
| [network_bind role](roles/network_bind/)       | Install or uninstall a bind service          |

## History

If you want to follow the collection history, you can follow the [release history](history.md).

## Improve and develop role

If you want to contribute to the startx inititive you can follow the [developper guidelines](developpers.md).

## Contributors

A full list of the contributors is available under the [contributors list page](contributors.md).
