# Infra : AWS CodeCommit role

The purpose of this role is to create, start, stop, deploy, profiles, info, delete and destroy an OCP/OKD aws_codecommit.
This role is part of the [STARTX infra ansible collection](https://galaxy.ansible.com/startxfr/infra).

## Requirements

- Ansible runtime
- Installation of the requirements with `ansible-galaxy install -r meta/requirements.yml`

## Role Variables

| Key                         | Default                      | Description                                         |
| --------------------------- | ---------------------------- | --------------------------------------------------- |
| ia_codecommit_action        | create                       | Action to perfom on the git repository              |
| ia_codecommit_name          | myrepository                 | Name of the repository (mandatory)                  |
| ia_codecommit_description   | description of my repository | Description of the repository                       |
| ia_codecommit_repoPrefix    | ""                           | Prefix to use for repo name                         |
| ia_codecommit_branch        | master                       | Branch used for as main branch                      |
| ia_codecommit_aws_accessKey | MY_KEY                       | AWS access key to use for codecommit administration |
| ia_codecommit_aws_secretKey | MY_SECRET                    | AWS secret key to use for codecommit administration |
| ia_codecommit_aws_region    | eu-west-3                    | AWS region to use for codecommit operations         |

## Dependencies

- `ansible.builtin` collection
- `community.aws` collection. See [aws community collection documentation](https://galaxy.ansible.com/community/aws)

## Example playbooks

### Create aws_codecommit playbook

Configure a git repository to store the new aws_codecommit definition.

```yaml
- name: Create aws_codecommit configuration
  hosts: localhost
  roles:
    - role: startxfr.infra.aws_codecommit
      vars:
        sx_aws_codecommit_action: create
        sx_aws_codecommit_config:
          name: myaws_codecommit
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
          baseDomain: myaws_codecommit.startx.fr
          pullSecret: { "auths": { "fake": { "auth": "aWQ6cGFzcwo=" } } }
          sshKey: "my_ssh_key"
```

### Deploy aws_codecommit playbook

Deploy a previously created environment

```yaml
- name: Deploy aws_codecommit
  hosts: localhost
  roles:
    - role: startxfr.infra.aws_codecommit
      vars:
        sx_aws_codecommit_action: deploy
        sx_aws_codecommit_config:
          name: myaws_codecommit
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
```

### Info aws_codecommit playbook

Get information about a aws_codecommit

```yaml
- name: Info aws_codecommit
  hosts: localhost
  roles:
    - role: startxfr.infra.aws_codecommit
      vars:
        sx_aws_codecommit_action: info
        sx_aws_codecommit_config:
          name: myaws_codecommit
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
```

### Destroy aws_codecommit playbook

Destroy aws_codecommit service.

```yaml
- name: destroy aws_codecommit
  hosts: localhost
  roles:
    - role: startxfr.infra.aws_codecommit
      vars:
        sx_aws_codecommit_action: destroy
        sx_aws_codecommit_config:
          name: myaws_codecommit
          scope: myscope
          environment: myenv
          owner: startx
          kind: managed
          profile: default
```
