# Infra : Network OpenVPN role

The purpose of this role is to installand delete an openvpn service.
This role is part of the [STARTX services ansible collection](https://galaxy.ansible.com/startxfr/services).

## Requirements

- Ansible runtime
- Installation of the [startx services collection](https://galaxy.ansible.com/startxfr/services) with `ansible-galaxy collection install startxfr.services`

## Role Variables

| Key                | Default | Description               |
| ------------------ | ------- | ------------------------- |
| in_openvpn_action  | create  | The action to perform     |
| in_openvpn_package | openvpn | Name of the proxy package |
| in_openvpn_daemon  | openvpn | Name of the proxy daemon  |

## Dependencies

Depend only on `ansible.builtin`

## Example playbooks

### Install openvpn playbook

Install openvpn service default version.

```yaml
- name: Install openvpn service
  hosts: localhost
  roles:
    - role: startxfr.services.network_openvpn
      vars:
        in_openvpn_action: create
```

### Uninstall openvpn playbook

Uninstall openvpn service default version.

```yaml
- name: Uninstall network_openvpn service
  hosts: localhost
  roles:
    - role: startxfr.services.network_openvpn
      vars:
        in_openvpn_action: delete
```
